package com.example.rafael.adocao.model.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rafael.adocao.R;
import com.example.rafael.adocao.model.Animal;
import com.example.rafael.adocao.model.DAO.AnimalDAO;
import com.example.rafael.adocao.model.DAO.UsuarioDAO;
import com.example.rafael.adocao.model.Usuario;

/**
 * Created by Rafael on 29/08/2017.
 */

public class UsuarioAdapter extends BaseAdapter {
    private Context context;
    private UsuarioDAO dao;

    public UsuarioAdapter(Context context, UsuarioDAO dao) {
        this.context = context;
        this.dao = dao;
    }

    @Override
    public int getCount() {
        return this.dao.size();
    }

    @Override
    public Object getItem(int position) {
        return this.dao.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
