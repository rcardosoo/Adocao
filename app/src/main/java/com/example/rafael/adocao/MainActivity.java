package com.example.rafael.adocao;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rafael.adocao.model.Adapter.AnimalAdapter;
import com.example.rafael.adocao.model.DAO.AnimalDAO;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private AnimalDAO animalDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    MainActivity.this.mostrarLista("home");
                    return true;
                case R.id.navigation_dashboard:
                    MainActivity.this.mostrarLista("dash");
                    return true;
            }
            return false;
        }
    };

    //carrega a listview de todos os animais para adoção no sistema
    private void mostrarLista(String page) {
        Log.i("TESTE", "ENTROU NO MOSTRARLISTA0000");
        this.animalDAO = new AnimalDAO(this);
        Log.i("TESTE", "ENTROU NO MOSTRARLISTA1111");
        this.listView = (ListView) findViewById(R.id.listView);
        Log.i("TESTE", "ENTROU NO MOSTRARLISTA2222");
        AnimalAdapter adapter = new AnimalAdapter(this, this.animalDAO);
        Log.i("TESTE", "ENTROU NO MOSTRARLISTA3333");
        this.listView.setAdapter(adapter);
        Log.i("TESTE", "ENTROU NO MOSTRARLISTA4444");

        //this.listViewMain.setOnItemClickListener(new ClickItemLista());
        //this.listViewMain.setOnItemLongClickListener(new ClickLongItemLista());
    }

}
